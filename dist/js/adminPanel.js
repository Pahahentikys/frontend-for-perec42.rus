/**
 * Created by Pavel on 21.08.2017.
 */
//Кастомный чекбокс в выборе категории или товара в категории по клику
$(document).ready(function () {
    // $('.catHolder').on('click', '.check', function () {
    //     if (!$(this).parent('.checkHolder').hasClass('na')) {
    //         if ($(this).parents('.checkHolder').hasClass('single')) {
    //             $(this).parents('.checkHolder').find('.check').removeClass('active');
    //             $(this).addClass('active');
    //         } else {
    //             $(this).toggleClass('active');
    //         }
    //     }
    // });

    $('body').on('click', function (e) {
        console.log(e);
    });
});


//Всплывающее окно по клику на кнопку создания новой категории
$(document).ready(function () {

    //Клил по кнопке: "добавить категорию товара"
    $('#buttonNewCategory').on('click', function () {

        $('.popupHolder.addCat').fadeIn();

    });

    //Отмена создания категории
    $('body').on('click', '.cancelEditCategoryPopup', function () {
        $('.popupHolder.addCat').fadeOut('fast', function () {
            // $(this).remove();
        });
    });

    $('body').on('click', '#saveCategoryPopup', function () {
        var messageErr = 'Введите название заголовка категории!';
        var messagePositive = 'Новая категория успешно создана!';
        var button = $(this);

        if (!button.hasClass('working')) {
            button.addClass('working');
            var data = {};
            data.title = $('#titleCategoryPopup').val();
            if (data.title.length == 0) {
                alert(messageErr);
                button.removeClass('working');
            } else {

                console.log("Данные для отправки на сервер: " + JSON.stringify(data));

                var serverResult =
                    '<li class="">' +
                    '<div class="text">' +
                    '<span class="title">' + data.title + '</span>' +
                    '<span class="small">Количество товаров: 0</span>' +
                    '</div>' +
                    '<div class="buttons">' +
                    '<div class="button edit editCat">Редактор</div>' +
                    '</div>' +
                    '</li>';
                button.removeClass('working');
                $('.popupHolder.addCat').fadeOut('fast', function () {
                    alert(messagePositive);
                });
                $('#sortable').append(serverResult);
            }
        }
    });
});

//Обработка формы: "Редактор категорий"
$(document).ready(function () {
    //Обработка клика по кнопке: "Удалить"
    $('#deleteThisСategory').on('click', function () {
        $('body').append('<div class="popupHolder deleteCategory">' +
            '<div class="popupContent">' +
            '<div class="popupHeader flex-centered">Удаление категории</div>' +
            '<div class="full textInPopup">Удалить данную категорию: ' + '<strong>' + $('#titleCatForEdit').val() +
            '</strong>' + '?' + '</div>' +
            '<div class="clear centered">' +
            '<div class="button del" id="deleteCategoryPopup">Удалить</div>' +
            '<div class="button" id="cancelDeleteCategoryPopup">Отменить</div>' +
            '</div>' +
            '</div>' +
            '</div>'
        );
        //Всплывающее окно
        $('.popupHolder.deleteCategory').fadeIn();
    });

    //Кнопка: "Отменить" в всплывающем окне
    $('body').on('click', '#cancelDeleteCategoryPopup', function () {
        $('.popupHolder.deleteCategory').fadeOut();
    });

    //Кнопка: "Удалить" во всплывающем окне
    $('#deleteCategoryPopup').on('click', function () {

        // var button = $(this);
        var button = $('#deleteThisСategory');
        if (!button.hasClass('working')) {
            button.addClass('working');
            //Запрос к серваку на удаление категории

        }
    });

    //Кнопка: "Сохранить"
    $('#saveEditedСategory').on('click', function () {

        var button = $(this);

        if (!button.hasClass('working')) {
            button.addClass('working');
            // if ($('#productsCategories .active').length > 0) {

            //Данные для отправки на сервер
            var data = {};
            data.titleCatForEdit = $('#titleCatForEdit').val();
            if (data.titleCatForEdit == 0) {
                alert('Поле с именем категории не должно быть пустым!');
                button.removeClass('working');
            } else {
                data.moderator = $('#moderator').text();

                // var listProductsInCat = [];
                // $('#productsCategories .active').each(function () {
                //     listProductsInCat.push($(this).text());
                // });
                // data.productsInCat = listProductsInCat;

                alert("Данные успешно сохранены!");
                console.log('Данные для сохранения: ' + JSON.stringify(data));

                //Когда запрос отправили
                button.removeClass('working');
            }
        }
        // else {
        //     alert('В категории должен быть хоят бы один товар!');
        //     button.removeClass('working');
        // }

    });
});

//Окно со списком товаров в редакторе категорий
$(document).ready(function () {
    //Клил по кнопке редактор в списке товаров в категории и клик
    $('.productEdit').on('click', function () {

        $('.popupHolder.editProduct').fadeIn();

    });

    // $('#editProductPopup')
    $('body').on('click', '#cancelEditProductPopup', function () {
        $('.popupHolder.editProduct').fadeOut();
    });

});

//Клик по кнопке: "Редактор в списке категорий". Разворачивается редактор категорий.
$(document).ready(function () {
    $('body').on('click', '.editCat', function () {

        var element = $('.slideCategoryEditor');

        if ($(element).hasClass('up')) {
            $(element).next().slideUp('fast');
            $(element).removeClass('up');
        } else {
            $(element).addClass('up');
            $(element).next().slideDown('fast');
        }

        //Клик по кнопке: "Отмена" в редакторе категорий
        $('body').on('click', '#cancelEditedCategory', function () {
            if ($('.categoryEditor').is(':visible')) {
                $('.categoryEditor').slideUp();
                $(element).removeClass('up');
            }
        });
    });
});

// //Подтверждение ухода со страницы
//
// window.beforeonload = function () {
//   return 'Вы уверены, что хотите покинуть страницу? Возможно, какие-то данные не были вами сохранены.'
// };
