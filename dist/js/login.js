/**
 * Created by Pavel on 26.08.2017.
 */
//Обработка формы логина для админки
$(document).ready(function () {
    var focusElem = $('#userLogin');
    focusElem.focus();

    var adminLoginReg = /^([a-zA-z0-9_\-])+$/;
    var adminPasswordReg = /^([a-zA-Z0-9!%&@#$\^*?_~+])+$/;

    $('#userPassword').on('keyup', function (e) {
        if (e.keyCode == 13) {
            $('#loginButton').click();
        }
    });

    $('#loginButton').on('click', function () {

        var data = {};
        data.userLogin = $('#userLogin').val();
        data.userPassword = $('#userPassword').val();
        if (data.userLogin == '' || data.userPassword == '') {
            alert('Поля с логином и паролем должны быть заполнены!');
        } else if (!adminLoginReg.test(data.userLogin)) {
            alert('Поле именем модератора содержит недопустимые символы!')
        } else if (!adminPasswordReg.test(data.userPassword)) {
            alert('Поле с паролем для модератора содержит недопустимые символы!')
        } else {
            //Запрос к серваку на авторизацию, пароль там посолить, тыры-пыры =)
            console.log('Данные для отправки на сервер: '+JSON.stringify(data));
        }
    });
});
