//Якорные ссылки
$(document).ready(function () {
    $('.mainNavWrap .mainNav').on('click', 'a', function (e) {
        e.preventDefault();
        var id = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 400);
    });
});

//Выпадающее меню
$(document).on('click', '.responsiveMenu', function () {
    $(this).toggleClass('active');
    if ($('.mainNav ul').is(':visible')) {
        $('.mainNav ul').slideUp();
    } else {
        $('.mainNav ul').slideDown();
    }
});

//Проявление кнопки наверх при пролистывании страницы вниз
$(window).scroll(function () {
    if ($(this).scrollTop() > $(this).height()) {
        $('.top').addClass('active');

    } else {
        $('.top').removeClass('active');
    }
});

//Скролл к верху страницы
var winHeight = $(document).height();
var step = 3;
var timeToScroll = winHeight / step;
$('.top').click(function () {
    $('html, body').stop().animate({
        scrollTop: 0
    }, timeToScroll);
});


//Всплывающее окно обратного вызова
$(document).ready(function () {
    var timeToHidePopup = 30000;
    var popupWindow = $('.popup');
    $('.clickForCall').on('click', function () {
        $('.popup').fadeIn(200);
        hidePopup(timeToHidePopup, popupWindow);
    });

    // $('.callMe').add('.close').on('click', (function () {
    //     $('.popup').fadeOut(200);
    // }));
    $('.close').on('click', (function () {
        $('.popup').fadeOut(200);
    }));

    $('body').on('click', function (e) {
        console.log(e);
    });
});

//Валидация формы обратного вызова
$(document).ready(function () {

    // var loginReg = /^([a-zA-z0-9_\-])+$/;
    var clientNameReg = /^[А-Яа-яё\s]{2,20}$|^[0-9a-zA-Z\s]{2,20}$/;
    var clientNumberPhoneReg = /^\+?[78][-\(]?\d{3}\)?-?\d{3}-?\d{2}-?\d{2}$/;

    $('#clientNumber').on('keyup', function (e) {
        var clickEnter = 13;
        if (e.keyCode == clickEnter) {
            $('.callMe').click();

        }
    });

    $('.callMe').on('click', function () {
        var popupWindow = $('.popup');
        var timeToHideWindow = 300;
        var messageEmptyName = 'Заполните поле с именем!';
        var messageEmptyNumber = 'Заполните поле с вашим номером телефона!';
        var messageIncorrectSymbolName = 'Вы ввели недопустимые символы в поле с именем: %, $, #, /, > и т.д.';
        var messageCallBack = 'Ваша заявка принята, ожидайте звонка нашего оператора. :)';
        var messageIncorrectSymbolPhone= 'Введите корректный номер телефона!';
        var data = {};
        data.clientName = $('#clientName').val();
        data.clientNumber = $('#clientNumber').val();

        if (data.clientName == '') {

            showMessageNegative(messageEmptyName);
        } else if (!clientNameReg.test(data.clientName)) {
            showMessageNegative(messageIncorrectSymbolName);
        } else if (data.clientNumber == '') {
            showMessageNegative(messageEmptyNumber);
        }else if(!clientNumberPhoneReg.test(data.clientNumber)){
            showMessageNegative(messageIncorrectSymbolPhone);
        } else {
            showMessagePositive(messageCallBack);
            console.log("Данные для отправки на сервер: "+ JSON.stringify(data));
            hidePopup(timeToHideWindow, popupWindow);
        }
    });
});

// Сокрытие всплывающего окна по таймеру
var timer = null; //время существования окна
function hidePopup(timeToHide, element) {

    console.log('таймер: ' + timer);

    if (timer != null) {
        window.clearTimeout(timer);
    }
    timer = window.setTimeout(function () {
        $(element).stop().animate().fadeOut(200);
    }, timeToHide);
}

//Окна оповещения добавления/удаления товара в корзину
function showMessagePositive(messageText) {
    var timeToHide = 1000;
    var popupAddWindow = $('.popupMessage');
    $('.popupMessage').removeClass('popupDeleteFromCart');
    $('.popupMessage').addClass('popupAddInCart');
    $('.popupMessage h2').text(messageText);
    $('.popupMessage').fadeIn(function () {
        hidePopup(timeToHide, popupAddWindow);
    });
}

function showMessageNegative(messageText) {
    var timeToHide = 1000;
    var popupDelWindow = $('.popupMessage');
    $('.popupMessage').removeClass('popupAddInCart');
    $('.popupMessage').addClass('popupDeleteFromCart');
    $('.popupMessage h2').text(messageText);
    $('.popupMessage').fadeIn(function () {
        hidePopup(timeToHide, popupDelWindow);
    });
}

//Скрыть уведомление по клику на окно уведомления
$(document).ready(function () {
    $('.popupMessage').on('click', function () {
        $(this).fadeOut(200);
    });
});

//Появление корзины в основном блоке по клику на кнопку: "в корзину"
$(document).ready(function () {
    var addProductMessage = 'Товар добавлен в корзину!';
    var cartGeneral = $('.cartGeneral');
    var btnAddInCart = $('.btnAddInCart');

    btnAddInCart.click(function () {
        cartGeneral.toggleClass('cartGeneralActive');
        showMessagePositive(addProductMessage);
    });
});

//Всплывающее меню корзины
$(document).ready(function () {
    var busket = $('.headerHideBusket');
    var busketActive = $('.hideBusketActive');
    var cartHide = $('.cartHide');
    var cartHideActive = $('.cartHideActive');
    var ctiticalSize = 551;
    var winWidth = $(document).width();

    $(window).resize(function () {
        if (winWidth = ctiticalSize) {
            cartHide.removeClass('cartHideActive')
        }
    });

    busket.click(function () {
        // busket.toggleClass('hideBusketActive');
        cartHide.toggleClass('cartHideActive');

    });
    busketActive.click(function () {
        busket.removeClass('hideBusketActive');
    });
});